如何将Springboot 应用打包成 Docker 镜像文件

**创建Spring Boot应用** 使用[Spring Initializr](https://start.spring.io/)来生成一个简单的Spring Boot项目。

以下是一个简单的Spring Boot项目的示例：

1. **项目结构**

```lua
springboot-demo/
|-- src/
|   |-- main/
|   |   |-- java/
|   |   |   |-- com.icoderoad/
|   |   |   |   |-- example.docker/
|   |   |   |   |   |-- SpringbootDemoApplication.java
|   |   |   |   |   |-- controller/
|   |   |   |   |   |   |-- HelloController.java
|   |-- resources/
|   |   |-- application.properties
|-- pom.xml
```

**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.example</groupId>
    <artifactId>springboot-demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>
    <name>springboot-demo</name>
    <description>Demo project for Spring Boot</description>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.5.4</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

**SpringbootDemoApplication.java**

```java
package com.icoderoad.example.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerApplication.class, args);
    }
   
}
```

**HelloController.java**

```java
package com.icoderoad.example.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello() {
        return "Hello, Spring Boot!";
    }
}
```

**application.properties**

```properties
server.port=8080
```

**添加Dockerfile到项目根目录** 创建一个名为`Dockerfile`的文件在Spring Boot项目的根目录。Dockerfile内容如下：

```dockerfile
FROM openjdk:11-jre-slim
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

此Dockerfile从OpenJDK 11的基础镜像开始，将编译好的JAR文件复制到镜像，并设置启动命令。

**编译Spring Boot项目** 在项目根目录下，使用Maven或Gradle进行编译：

Maven:

```shell
mvn clean package
```

**构建Docker镜像** 首先确保系统上已经安装了Docker。然后在项目的根目录下运行：

```shell
docker build -t your-springboot-app-name .
```

这会根据`Dockerfile`构建一个名为`your-springboot-app-name`的Docker镜像。

**运行Docker容器** 创建的镜像可以使用以下命令运行：

```shell
docker run -p 8080:8080 your-springboot-app-name
```

这将启动容器，并将容器的8080端口映射到宿主机的8080端口。可以通过访问`http://localhost:8080`来访问应用。

完成以上步骤后，我们就成功地将Spring Boot应用打包到了Docker镜像中，并能够运行该镜像作为一个容器。