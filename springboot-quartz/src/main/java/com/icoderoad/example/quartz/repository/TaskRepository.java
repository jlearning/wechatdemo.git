package com.icoderoad.example.quartz.repository;

import org.springframework.data.repository.CrudRepository;

import com.icoderoad.example.quartz.entity.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {
}