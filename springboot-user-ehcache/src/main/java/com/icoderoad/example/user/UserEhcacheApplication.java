package com.icoderoad.example.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserEhcacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserEhcacheApplication.class, args);
	}

}