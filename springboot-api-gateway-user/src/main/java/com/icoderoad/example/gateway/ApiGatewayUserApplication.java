package com.icoderoad.example.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGatewayUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayUserApplication.class, args);
    }
}