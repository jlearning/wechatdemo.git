package com.icoderoad.example.gateway.controller;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icoderoad.example.gateway.entity.User;

@RestController
@RequestMapping("/api/users")
public class UserController {

    // 模拟用户数据
    private final Map<Long, User> userDatabase = new ConcurrentHashMap<>();

    // 添加一些示例用户
    public UserController() {
        userDatabase.put(1L, new User(1L, "John Doe", "johndoe@example.com"));
        userDatabase.put(2L, new User(2L, "Jane Smith", "janesmith@example.com"));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<User> getUser(@PathVariable Long userId) {
        User user = userDatabase.get(userId);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}