package com.icoderoad.example.file.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class TokenService {
    private Map<String, String> tokenMap = new HashMap<>();

    public void storeToken(String fileName, String token) {
        tokenMap.put(fileName, token);
    }

    public String getToken(String fileName) {
        return tokenMap.get(fileName);
    }
}