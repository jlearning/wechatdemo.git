package com.icoderoad.example.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserRedisApplication.class, args);
	}

}